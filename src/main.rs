extern crate nom;
extern crate cse262_project;

use cse262_project::program;

fn main() {
  let result = program(r#"fn main(){return foo();} fn foo(){return 5;}"#);
  println!("{:?}", result);
}
